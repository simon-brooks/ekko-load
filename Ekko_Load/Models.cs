﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekko_Load.models
{
    public class Rule
    {
        public int id { get; set; }
        public string name { get; set; }
        public int parent { get; set; }
        public List<Rules> rules { get; set; }
    }

    public class Rules
    {
        public int ruleId { get; set; }
        public string speaker { get; set; }
        public bool include { get; set; }
        public string words { get; set; }
        public int spacing { get; set; }
    }

    public class Words : ICloneable
    {
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        public int p { get; set; }
        public int s { get; set; }
        public float c { get; set; }
        public float v { get; set; }
        public int e { get; set; }
        public string w { get; set; }
        public string m { get; set; }
        public Frqs[] frq { get; set; }
    }

    public class Frqs
    {
        public float f { get; set; }
        public float e { get; set; }
    }

    public class PWords
    {
        public int p { get; set; }
        public int s { get; set; }
        public string w { get; set; }
        public string m { get; set; }
    }

    public class PUpload
    {
        public string mediaId { get; set; }
        public string action { get; set; }
        public PWords[] content { get; set; }
    }


    public class Diarization
    {
        public string speakerlabel { get; set; }
        public string band { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public string gender { get; set; }
        public string env { get; set; }
    }


    public class Latest
    {
        public Diarization[] diarization { get; set; }
        public Words[] words { get; set; }
    }


    public class Transcripts
    {
        public Latest latest { get; set; }
    }

    public class keyt
    {
        public float[] unknown { get; set; }
    }

    public class Wordskey
    {
        public string name { get; set; }
        public keyt t { get; set; }
        public float relevance { get; set; }
        public string[] internalName { get; set; }
        public float score { get; set; }
    }

    public class Latestkey
    {
        public Wordskey[] words { get; set; }
    }

    public class Keywords
    {
        public Latestkey latest { get; set; }
    }

    public class TopicsTop
    {
        public float score { get; set; }
        public Wordskey[] keywords { get; set; }
        public string name { get; set; }

    }

    public class LatestTop
    {
        public TopicsTop[] topics { get; set; }
    }

    public class Topics
    {
        public LatestTop latest { get; set; }
    }

    public class Length
    {
        public int milliseconds { get; set; }
        public string descriptive { get; set; }
    }

    public class Metadata
    {
        public Length length { get; set; }
    }

    public class MediaResponse
    {
        public string mediaId { get; set; }
        public string status { get; set; }
        public Transcripts transcripts { get; set; }
        public Keywords keywords { get; set; }
        public Topics topics { get; set; }
        public Metadata metadata { get; set; }
        public string dateFinished { get; set; }
        public string dateCreated { get; set; }
        //       public string message { get; set; }
    }

    public class Media
    {
        //        public string media { get; set; }
        public MediaResponse media { get; set; }
    }

    public class ResponseData
    {
        public string requestStatus { get; set; }
        public int? id { get; set; }

    }

    public class RuleItem
    {
        public string match { get; set; }
        public int query_id { get; set; }
        public int actual_slop { get; set; }
        public int time { get; set; }
        public string speaker { get; set; }
    }
    public class rules
    {
        public Dictionary<string, List<RuleItem>> TopicArray { get; set; }
    }

    public class quality
    {
        //        public topics exact { get; set; } 
        public Dictionary<string, List<RuleItem>> mono { get; set; }
        public Dictionary<string, List<RuleItem>> agent { get; set; }
        public Dictionary<string, List<RuleItem>> caller { get; set; }


    }
    public class topics
    {
        public Dictionary<string, List<float>> items { get; set; }
    }


    public class topic
    {
        public topics topics { get; set; }
        public string model_name { get; set; }
        public string transcript_id { get; set; }

    }

    public class st_overtalk_turn
    {
        public int turn_count { get; set; }
        public float turn_seconds { get; set; }
        public float turn_percentage { get; set; }
    }
    public class st_overtalk
    {
        public st_overtalk_turn agent { get; set; }
        public st_overtalk_turn caller { get; set; }
    }

    public class st_pauses_type
    {
        public int turn_count { get; set; }
        public float turn_seconds { get; set; }
        public float turn_percentage { get; set; }
    }

    public class st_pauses
    {
        public st_pauses_type agent { get; set; }
        public st_pauses_type caller { get; set; }
    }

    public class st_speaker_speed
    {
        public float agent { get; set; }
        public float caller { get; set; }
        public float mono { get; set; }
    }


    public class st_change_turn
    {
        public int turn_index { get; set; }
        public int timestamp { get; set; }
        public float relative_timestamp { get; set; }
        public float speaker_change_duration_sec { get; set; }
        public string speaker_change_type { get; set; }
    }

    public class st_change
    {
        public st_change_turn[] agent { get; set; }
        public st_change_turn[] caller { get; set; }
    }

    public class stats
    {
        public string ID { get; set; }
        public int duration { get; set; }
        public int num_tokens { get; set; }
        public float agent_talk_percentage { get; set; }
        public float caller_talk_percentage { get; set; }
        public float interruptions_percentage { get; set; }
        public float pauses_percentage { get; set; }
        public st_overtalk overtalk { get; set; }
        public st_pauses pauses { get; set; }
        public st_speaker_speed speaker_words_per_minute { get; set; }
        public st_speaker_speed speaker_words_per_sentence { get; set; }
        public st_change speaker_change { get; set; }

    }

    public class distribution
    {
        public float[] time { get; set; }
        public float[] sentiment { get; set; }
    }
    public class distributions
    {
        public distribution mono { get; set; }
        public distribution agent { get; set; }
        public distribution caller { get; set; }
    }

    public class bucket
    {
        public int index { get; set; }
        public float value { get; set; }
    }
    public class bucket_distributions
    {
        public bucket[] mono_time_bucket { get; set; }
        public bucket[] agent_time_bucket { get; set; }
        public bucket[] caller_time_bucket { get; set; }
        public bucket[] mono_bucket { get; set; }
        public bucket[] agent_bucket { get; set; }
        public bucket[] caller_bucket { get; set; }
    }

    public class aggregate
    {
        public float mono { get; set; }
        public float agent { get; set; }
        public float caller { get; set; }
    }

    public class trend
    {
        public aggregate slope { get; set; }
        public aggregate intercept { get; set; }
    }
    public class trends
    {
        public trend overall_trend { get; set; }
        public trend bucketed_time_trend { get; set; }
        public trend bucketed_trend { get; set; }
    }
    public class sentiment
    {
        public string transcript_id { get; set; }
        public distributions distributions { get; set; }
        public bucket_distributions time_bucket_distributions { get; set; }
        public bucket_distributions bucketed_distributions { get; set; }
        public aggregate averages { get; set; }
        public trends trends { get; set; }
    }

}
