﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data.SqlClient;
using System.Data;
using Ekko_Load.models;
using System.Web.Script.Serialization;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using ExtendedXmlSerializer.Configuration;
using System.Xml.Serialization;
using System.ComponentModel;

namespace Ekko_Load
{
    class Program
    {
        static void Main(string[] args)
        {
            NpgsqlConnection ekkoDbConnection = new NpgsqlConnection("Server=pg-daisee-ekko-test.cquvuslt1stb.ap-southeast-2.rds.amazonaws.com;Port=5432;User Id=ekko_app_user;Password=Pa$$word0123;Database=ekko_IPA;");
            ekkoDbConnection.Open();
            SqlConnection tqDbConnection = new SqlConnection("Server = tcp:torquexdb1.database.windows.net,1433; Data Source = torquexdb1.database.windows.net; Initial Catalog = TorquexDB1; Persist Security Info = False; User ID =Simon; Password =Jasper123; Pooling = False; MultipleActiveResultSets = True; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;");
            tqDbConnection.Open();

//            LoadDxc(tqDbConnection, ekkoDbConnection);

//            LoadDimetro(@"P:\News POC June 2016\Output Files\Datacom\IPA\metadata.csv", ekkoDbConnection);

            string baseDir = @"P:\News POC June 2016\Output Files\dxc\DxC_ceres_output";

//            ReLoadWords(@"P:\News POC June 2016\Output Files\Datacom\IPA\datacom_qm\datacom_qm", ekkoDbConnection);

            //            LoadCeres(baseDir, ekkoDbConnection);
             //           LoadCeresTokenWrapper(baseDir, ekkoDbConnection);
//            baseDir = @"P:\News POC June 2016\Output Files\dxc\Updated_Match\20180608T001919";
//            LoadCeresQualityWrapper(@"P:\News POC June 2016\Output Files\Datacom\IPA\datacom_qm\datacom_qm", ekkoDbConnection);
             LoadQM(ekkoDbConnection, @"P:\News POC June 2016\Output Files\Datacom\IPA\datacom_qm\Quality_scorecards_Datacom.csv");

        }

        static void ReLoadWords(string basedir, NpgsqlConnection ekkoDbConnection)
        {
            Guid trans_id;
            string json = "";

            foreach (string file in Directory.GetFiles(basedir))
            {
                json = File.ReadAllText(file);
                var json_serializer = new JavaScriptSerializer();
                Media VBJson = json_serializer.Deserialize<Media>(json);
                trans_id = GetTranscriptId(ekkoDbConnection, VBJson.media.mediaId);
                LoadTranscriptWord(file, ekkoDbConnection, trans_id);


            }
        }
        static void LoadCeresQualityWrapper(string basedir, NpgsqlConnection ekkoDbConnection)
        {
            string[] directories = Directory.GetDirectories(basedir);
            foreach (string dir in directories)
            {
                //                Guid transcript_id = GetTranscriptId(ekkoDbConnection, new DirectoryInfo(dir).Name);
                Guid transcript_id;
                string dirname = new DirectoryInfo(dir).Name;
                NpgsqlCommand sql_trans = new NpgsqlCommand("SELECT t.transcript_id from transcript t join dm_recording_file rf on t.recording_file_id = rf.recording_file_id  where rf.filename = '" + dirname + ".mp3' ", ekkoDbConnection);
                try
                {
                    transcript_id = (Guid)sql_trans.ExecuteScalar();
                }
                catch (Exception e)
                {
                    continue;
                }
                if (transcript_id == Guid.Empty)
                {
                    continue;

                }
                Guid ceres_run_id;

                // Create ceres run record
                NpgsqlCommand sql = new NpgsqlCommand("SELECT cr.id from ceres_run cr  where transcript_id = '" + transcript_id + "' ", ekkoDbConnection);
                try
                {
                    ceres_run_id = (Guid)sql.ExecuteScalar();
                }
                catch (Exception e)
                {
                    continue;
                }

                if (File.Exists(dir + @"\quality.json"))
                {
                    Guid ceres_process_id = InsertCeresProcess(ekkoDbConnection, ceres_run_id, "quality");
                    LoadRuleData(ekkoDbConnection, ceres_process_id, dir);
                }



            }
        }




        static void LoadCeresTokenWrapper(string basedir, NpgsqlConnection ekkoDbConnection)
        {
            string[] directories = Directory.GetDirectories(basedir);
            foreach (string dir in directories)
            {
                Guid transcript_id = GetTranscriptId(ekkoDbConnection, new DirectoryInfo(dir).Name);
                if (transcript_id == Guid.Empty)
                {
                    continue;

                }
                Guid ceres_run_id;

                // Create ceres run record
                NpgsqlCommand sql = new NpgsqlCommand("SELECT cr.id from ceres_run cr join transcript t on (cr.transcript_id = t.id) where t.ref_id = '" + transcript_id + "' ", ekkoDbConnection);
                try
                {
                    ceres_run_id = (Guid)sql.ExecuteScalar();
                }
                catch (Exception e)
                {
                    continue;
                }

                if (File.Exists(dir + @"\pos_tags.csv"))
                {
                    Guid ceres_process_id = InsertCeresProcess(ekkoDbConnection, ceres_run_id, "pos_tags");
                    InsertTokens(ekkoDbConnection, dir, transcript_id);

                }



            }
        }

        static void LoadQM(NpgsqlConnection ekkoDbConnection, string path)
        {
            using (TextFieldParser csvParser = new TextFieldParser(path))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = true;

                // Skip the row with the column names
                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();
                    string transcript_id = fields[0];
                    int i = 1;
                    while (i < 6)
                    {
                        if (fields[i] == "") fields[i] = "0.0";
                        i++;
                    }
                    double qm1 = Convert.ToDouble(fields[1]);
                    double qm2 = Convert.ToDouble(fields[2]);
                    double qm3 = Convert.ToDouble(fields[3]);
                    double qm4 = Convert.ToDouble(fields[4]);
                    double qm5 = Convert.ToDouble(fields[5]);
                    NpgsqlCommand sql = new NpgsqlCommand("SELECT cr.id from ceres_run cr " +
                                                            " join transcript t on (cr.transcript_id = t.transcript_id) " +
                                                            " join dm_recording_file rf on (t.recording_file_id = rf.recording_file_id) " + 
                                                            " where rf.filename = '" + transcript_id + ".mp3' ", ekkoDbConnection);
                    try
                    {
                        Guid run_id = (Guid)sql.ExecuteScalar();
                        Guid ceres_process_id = InsertCeresProcess(ekkoDbConnection, run_id, "qm");
                        NpgsqlCommand insert = new NpgsqlCommand();
                        insert.Connection = ekkoDbConnection;
                        insert.CommandText = "INSERT INTO ceres_qm (" +
                                                " id, " +
                                                " ceres_process_id," +
                                                " qm_01," +
                                                " qm_02," +
                                                " qm_03," +
                                                " qm_04," +
                                                " qm_05) VALUES ('" +
                                                Guid.NewGuid().ToString() + "'," +
                                                "@ceres_process_id," +
                                                "@qm_01," +
                                                "@qm_02," +
                                                "@qm_03," +
                                                "@qm_04," +
                                                "@qm_05)";
                        insert.Parameters.AddWithValue("@ceres_process_id", ceres_process_id);
                        insert.Parameters.AddWithValue("@qm_01", qm1);
                        insert.Parameters.AddWithValue("@qm_02", qm2);
                        insert.Parameters.AddWithValue("@qm_03", qm3);
                        insert.Parameters.AddWithValue("@qm_04", qm4);
                        insert.Parameters.AddWithValue("@qm_05", qm5);
                        insert.ExecuteNonQuery();
                        continue;
                    }
                    catch (Exception e)
                    {
                        //skip
                        continue;


                    }

                }
            }

        }

        static void InsertTokens(NpgsqlConnection ekkoDbConnection, string dir, Guid transcript_id)
        {
            string path = (dir + @"\pos_tags.csv");
            string[] fields;
            int pos;
            string token;
            string ttype;

            using (TextFieldParser csvParser = new TextFieldParser(path))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { ";" });
                csvParser.HasFieldsEnclosedInQuotes = true;

                // Skip the row with the column names
                //csvParser.ReadLine();

                NpgsqlCommand sql = new NpgsqlCommand("SELECT id from transcript_word where transcript_id = @transcript_id and position = @position", ekkoDbConnection);
                sql.Parameters.Add("@transcript_id",NpgsqlTypes.NpgsqlDbType.Uuid);
                sql.Parameters.Add("@position",NpgsqlTypes.NpgsqlDbType.Integer);

                NpgsqlCommand insert = new NpgsqlCommand();
                insert.Connection = ekkoDbConnection;
                insert.CommandText = "INSERT INTO ceres_tokens (" +
                                        " id, " +
                                        " word_id," +
                                        " word_position," +
                                        " token_position," +
                                        " token," +
                                        " part_of_speech) VALUES (" +
                                        "@id," +
                                        "@word_id," +
                                        "@pos," +
                                        "@pos," +
                                        "@token," +
                                        "@part_of_speech)";
                insert.Parameters.Add("@id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insert.Parameters.Add("@word_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insert.Parameters.Add("@pos", NpgsqlTypes.NpgsqlDbType.Integer);
                insert.Parameters.Add("@token", NpgsqlTypes.NpgsqlDbType.Varchar);
                insert.Parameters.Add("@part_of_speech", NpgsqlTypes.NpgsqlDbType.Varchar);


                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    fields = csvParser.ReadFields();
                    pos = Int32.Parse(fields[0]);
                    token = fields[1];
                    ttype = fields[2];

                    sql.Parameters["@transcript_id"].Value = transcript_id;
                    sql.Parameters["@position"].Value = pos;

                    try
                    {
                        Guid word_id = (Guid)sql.ExecuteScalar();
                        insert.Parameters["@id"].Value = Guid.NewGuid().ToString();
                        insert.Parameters["@word_id"].Value = word_id;
                        insert.Parameters["@pos"].Value = pos;
                        insert.Parameters["@token"].Value = token;
                        insert.Parameters["@part_of_speech"].Value = ttype;
                        insert.ExecuteNonQuery();
                        continue;
                    }
                    catch (Exception e)
                    {
                        //skip
                        continue;


                    }

                }
            }


        }


        static void LoadCeres(string basedir, NpgsqlConnection ekkoDbConnection)
        {
            string[] directories = Directory.GetDirectories(basedir);
            foreach (string dir in directories)
            {

                Guid transcript_id = GetTranscriptId(ekkoDbConnection, new DirectoryInfo(dir).Name);
                if (transcript_id == Guid.Empty)
                {
                    continue;

                }
                Guid ceres_run_id; 

                // Create ceres run record
                NpgsqlCommand sql = new NpgsqlCommand("SELECT id from ceres_run where transcript_id = '" + transcript_id + "'", ekkoDbConnection);
                try
                {
                    ceres_run_id = (Guid)sql.ExecuteScalar();
//                    continue;
                }
                catch (Exception e)
                {
                    ceres_run_id = Guid.NewGuid();
                    NpgsqlCommand insert_run = new NpgsqlCommand();
                    insert_run.Connection = ekkoDbConnection;
                    insert_run.CommandText = "INSERT INTO ceres_run " +
                                                "(id " +
                                                  ",transcript_id) VALUES ( @runId, @transcriptId ) ";
                    insert_run.Parameters.AddWithValue("@runId", ceres_run_id);
                    insert_run.Parameters.AddWithValue("@transcriptId", transcript_id);
                    insert_run.ExecuteNonQuery();

                }


                if (File.Exists(dir + @"\pos_tags.csv"))
                {
//                    Guid ceres_process_id = InsertCeresProcess(ekkoDbConnection, ceres_run_id, "pos_tags");
//                    InsertTokens(ekkoDbConnection, dir, transcript_id);
                }


 /*               if (File.Exists(dir+@"\stats.json"))
                {
                    Guid ceres_process_id = InsertCeresProcess(ekkoDbConnection, ceres_run_id, "stats");
                    InsertStats(ekkoDbConnection, dir, ceres_process_id);
                }

                if (File.Exists(dir + @"\topics.json"))
                {
                    Guid ceres_process_id = InsertCeresProcess(ekkoDbConnection, ceres_run_id, "topics");
                    InsertTopics(ekkoDbConnection, ceres_process_id, dir);
                }

                if (File.Exists(dir + @"\quality.json"))
                {
                    Guid ceres_process_id = InsertCeresProcess(ekkoDbConnection, ceres_run_id, "quality");
                    LoadRuleData(ekkoDbConnection, ceres_process_id, dir);
                }*/

                if (File.Exists(dir + @"\sentiment.json"))
                {
                   Guid ceres_process_id = InsertCeresProcess(ekkoDbConnection, ceres_run_id, "sentiment");
                   LoadSentiment(ekkoDbConnection, ceres_process_id, dir);
                }


            }
        }

        static void LoadSentiment(NpgsqlConnection ekkoDbConnection, Guid ceres_process_id, string dir)
        {
            string JSON = File.ReadAllText(dir + @"\sentiment.json");
            var json_searializer = new JavaScriptSerializer();
            sentiment sent = json_searializer.Deserialize<sentiment>(JSON);
            if (sent.distributions != null) {
                if (sent.distributions.mono != null)
                {
                    loadsentdist(ekkoDbConnection, ceres_process_id, sent.distributions.mono, "mono");
                }
                if (sent.distributions.agent != null)
                {
                    loadsentdist(ekkoDbConnection, ceres_process_id, sent.distributions.agent, "agent");
                }
                if (sent.distributions.caller != null)
                {
                    loadsentdist(ekkoDbConnection, ceres_process_id, sent.distributions.caller, "caller");
                }
            }
            if (sent.bucketed_distributions != null) {
                if (sent.bucketed_distributions.agent_bucket != null)
                {
                    loadsentbucket(ekkoDbConnection, ceres_process_id, sent.bucketed_distributions.agent_bucket, "agent", "bucket");
                }
                if (sent.bucketed_distributions.caller_bucket != null)
                {
                    loadsentbucket(ekkoDbConnection, ceres_process_id, sent.bucketed_distributions.caller_bucket, "caller", "bucket");
                }
                if (sent.bucketed_distributions.mono_bucket != null)
                {
                    loadsentbucket(ekkoDbConnection, ceres_process_id, sent.bucketed_distributions.mono_bucket, "mono", "bucket");
                }
            }
            if (sent.time_bucket_distributions != null)
            {
                    if (sent.time_bucket_distributions.agent_time_bucket != null)
                {
                    loadsentbucket(ekkoDbConnection, ceres_process_id, sent.time_bucket_distributions.agent_time_bucket, "agent", "time_bucket");
                }
                if (sent.time_bucket_distributions.caller_time_bucket != null)
                {
                    loadsentbucket(ekkoDbConnection, ceres_process_id, sent.time_bucket_distributions.caller_time_bucket, "caller", "time_bucket");
                }
                if (sent.time_bucket_distributions.mono_time_bucket != null)
                {
                    loadsentbucket(ekkoDbConnection, ceres_process_id, sent.time_bucket_distributions.mono_time_bucket, "mono", "time_bucket");
                }
            }
            if (sent.trends != null && sent.averages != null)
            {
                NpgsqlCommand insert = new NpgsqlCommand();
                insert.Connection = ekkoDbConnection;
                insert.CommandText = "INSERT INTO ceres_sentiment_summary( " +
                                        "ceres_process_id, average_mono, average_agent, average_caller, " +
                                        "overall_trend_slope_mono, overall_trend_slope_agent, overall_trend_slope_caller, " +
                                        "overall_trend_intercept_mono, overall_trend_intercept_agent, " +
                                        "overall_trend_intercept_caller, bucketed_time_trend_slope_mono, " +
                                        "bucketed_time_trend_slope_agent, bucketed_time_trend_slope_caller, " +
                                        "bucketed_time_trend_intercept_mono, bucketed_time_trend_intercept_agent, " +
                                        "bucketed_time_trend_intercept_caller, bucketed_trend_slope_mono, " +
                                        "bucketed_trend_slope_agent, bucketed_trend_slope_caller, bucketed_trend_intercept_mono, " +
                                        "bucketed_trend_intercept_agent, bucketed_trend_intercept_caller) " +
                                        " VALUES(" +
                                        "@ceres_process_id, " +
                                        "@average_mono, " +
                                        "@average_agent, " +
                                        "@average_caller, " +
                                        "@overall_trend_slope_mono, " +
                                        "@overall_trend_slope_agent, " +
                                        "@overall_trend_slope_caller, " +
                                        "@overall_trend_intercept_mono, " +
                                        "@overall_trend_slope_agent, " +
                                        "@overall_trend_intercept_caller, " +
                                        "@bucketed_time_trend_slope_mono, " +
                                        "@bucketed_time_trend_slope_agent, " +
                                        "@bucketed_time_trend_slope_caller, " +
                                        "@bucketed_time_trend_intercept_caller, " +
                                        "@bucketed_time_trend_intercept_agent, " +
                                        "@bucketed_time_trend_intercept_caller, " +
                                        "@bucketed_trend_slope_mono, " +
                                        "@bucketed_trend_slope_agent, " +
                                        "@bucketed_trend_slope_caller, " +
                                        "@bucketed_trend_intercept_mono, " +
                                        "@bucketed_trend_intercept_agent, " +
                                        "@bucketed_trend_intercept_caller) ";
                insert.Parameters.AddWithValue("@ceres_process_id", ceres_process_id);
                insert.Parameters.AddWithValue("@average_mono", sent.averages.mono);
                insert.Parameters.AddWithValue("@average_agent", sent.averages.agent);
                insert.Parameters.AddWithValue("@average_caller", sent.averages.caller);
                insert.Parameters.AddWithValue("@overall_trend_slope_mono", sent.trends.overall_trend.slope.mono);
                insert.Parameters.AddWithValue("@overall_trend_slope_agent", sent.trends.overall_trend.slope.agent);
                insert.Parameters.AddWithValue("@overall_trend_slope_caller", sent.trends.overall_trend.slope.caller);
                insert.Parameters.AddWithValue("@overall_trend_intercept_mono", sent.trends.overall_trend.intercept.mono);
                insert.Parameters.AddWithValue("@overall_trend_slope_agent", sent.trends.overall_trend.intercept.agent);
                insert.Parameters.AddWithValue("@overall_trend_intercept_caller", sent.trends.overall_trend.intercept.caller);
                insert.Parameters.AddWithValue("@bucketed_time_trend_slope_mono", sent.trends.bucketed_time_trend.slope.mono);
                insert.Parameters.AddWithValue("@bucketed_time_trend_slope_agent", sent.trends.bucketed_time_trend.slope.agent);
                insert.Parameters.AddWithValue("@bucketed_time_trend_slope_caller", sent.trends.bucketed_time_trend.slope.caller);
                insert.Parameters.AddWithValue("@bucketed_time_trend_intercept_caller", sent.trends.bucketed_time_trend.intercept.mono);
                insert.Parameters.AddWithValue("@bucketed_time_trend_intercept_agent", sent.trends.bucketed_time_trend.intercept.agent);
                insert.Parameters.AddWithValue("@bucketed_time_trend_intercept_caller", sent.trends.bucketed_time_trend.intercept.caller);
                insert.Parameters.AddWithValue("@bucketed_trend_slope_mono", sent.trends.bucketed_trend.slope.mono);
                insert.Parameters.AddWithValue("@bucketed_trend_slope_agent", sent.trends.bucketed_trend.slope.agent);
                insert.Parameters.AddWithValue("@bucketed_trend_slope_caller", sent.trends.bucketed_trend.slope.caller);
                insert.Parameters.AddWithValue("@bucketed_trend_intercept_mono", sent.trends.bucketed_trend.intercept.mono);
                insert.Parameters.AddWithValue("@bucketed_trend_intercept_agent", sent.trends.bucketed_trend.intercept.agent);
                insert.Parameters.AddWithValue("@bucketed_trend_intercept_caller", sent.trends.bucketed_trend.intercept.caller);
                insert.ExecuteNonQuery();
            }



        }

        static void loadsentdist(NpgsqlConnection ekkoDbConnection, Guid ceres_process_id, distribution dist, string dist_type)
        {
            NpgsqlCommand insert = new NpgsqlCommand();
            insert.Connection = ekkoDbConnection;
            insert.CommandText = "INSERT INTO ceres_sentiment_distribution " +
                                    "(ceres_process_id " +
                                      ",speaker " +
                                      ",time " +
                                      ",value) VALUES " +
                                  "(@ceres_process_id " +
                                   ",@speaker " +
                                   ",@time " +
                                   ",@value) ";
            insert.Parameters.AddWithValue("@ceres_process_id", ceres_process_id);
            insert.Parameters.AddWithValue("@speaker", dist_type);
            insert.Parameters.Add("@time", NpgsqlTypes.NpgsqlDbType.Real);
            insert.Parameters.Add("@value", NpgsqlTypes.NpgsqlDbType.Real);
            for (int i = 0; i < dist.time.Length; i++)
            {
                insert.Parameters["@time"].Value = dist.time[i];
                insert.Parameters["@value"].Value = dist.sentiment[i];
                insert.ExecuteNonQuery();

            }
        }

        static void loadsentbucket(NpgsqlConnection ekkoDbConnection, Guid ceres_process_id, bucket[] bucket_array, string speaker, string bucket_name)
        {
            NpgsqlCommand insert = new NpgsqlCommand();
            insert.Connection = ekkoDbConnection;
            insert.CommandText = "INSERT INTO ceres_sentiment_bucket " +
                                    "(ceres_process_id " +
                                      ",speaker " +
                                      ",index " +
                                      ",bucket_type " +
                                      ",value) VALUES " +
                                  "(@ceres_process_id " +
                                   ",@speaker " +
                                   ",@index " +
                                   ",@bucket_type " +
                                   ",@value) ";
            insert.Parameters.AddWithValue("@ceres_process_id", ceres_process_id);
            insert.Parameters.AddWithValue("@speaker", speaker);
            insert.Parameters.AddWithValue("@bucket_type", bucket_name);
            insert.Parameters.Add("@index", NpgsqlTypes.NpgsqlDbType.Integer);
            insert.Parameters.Add("@value", NpgsqlTypes.NpgsqlDbType.Real);
            foreach (bucket b in bucket_array)
            {
                insert.Parameters["@index"].Value = b.index;
                insert.Parameters["@value"].Value = b.value;
                insert.ExecuteNonQuery();

            }
        }


        static void InsertStats(NpgsqlConnection ekkoDbConnection, string dir, Guid ceres_process_id)
        {
            string JSON = File.ReadAllText(dir + @"\stats.json");
            var json_searializer = new JavaScriptSerializer();
            stats cers_stats_JSON = json_searializer.Deserialize<stats>(JSON);

            NpgsqlCommand insert = new NpgsqlCommand();
            insert.Connection = ekkoDbConnection;
            insert.CommandText = "INSERT INTO ceres_stats " +
"(ceres_process_id " +
  ",duration " +
  ",num_tokens " +
  ",agent_talk_percentage " +
  ",caller_talk_percentage " +
  ",interruptions_percentage " +
  ",pauses_percentage " +
  ",overtalk_agent_count " +
  ",overtalk_agent_seconds " +
  ",overtalk_agent_percentage " +
  ",overtalk_caller_count " +
  ",overtalk_caller_seconds " +
  ",overtalk_caller_percentage " +
  ",pauses_agent_count " +
  ",pauses_agent_seconds " +
  ",pauses_agent_percentage " +
  ",pauses_caller_count " +
  ",pauses_caller_seconds " +
  ",pauses_caller_percentage " +
  ",words_per_minute_agent " +
  ",words_per_minute_caller " +
  ",words_per_minute_mono " +
  ",words_per_sentence_agent " +
  ",words_per_sentence_caller " +
  ",words_per_sentence_mono) " +
"VALUES " +
  "(@refId " +
   ",@duration " +
   ",@num_tokens " +
   ",@agent_talk_percentage " +
   ",@caller_talk_percentage " +
   ",@interruptions_percentage " +
   ",@pauses_percentage " +
   ",@agent_overtalk_turns " +
   ",@agent_overtalk_time " +
   ",@agent_overtalk_percentage " +
   ",@caller_overtalk_turns " +
   ",@caller_overtalk_time " +
   ",@caller_overtalk_percentage " +
   ",@agent_pauses_turns " +
   ",@agent_pauses_time " +
   ",@agent_pauses_percentage " +
   ",@caller_pauses_turns " +
   ",@caller_pauses_time " +
   ",@caller_pauses_percentage " +
   ",@agent_words_per_minute " +
   ",@caller_words_per_minute " +
   ",@mono_words_per_minute " +
   ",@agent_words_per_sentence " +
   ",@caller_words_per_sentence " +
   ",@mono_words_per_sentence) ";


            insert.Parameters.AddWithValue("@refId", ceres_process_id);
            insert.Parameters.AddWithValue("@duration", cers_stats_JSON.duration);
            insert.Parameters.AddWithValue("@num_tokens", cers_stats_JSON.num_tokens);
            insert.Parameters.AddWithValue("@agent_talk_percentage", cers_stats_JSON.agent_talk_percentage);
            insert.Parameters.AddWithValue("@caller_talk_percentage", cers_stats_JSON.caller_talk_percentage);
            insert.Parameters.AddWithValue("@interruptions_percentage", cers_stats_JSON.interruptions_percentage);
            insert.Parameters.AddWithValue("@pauses_percentage", cers_stats_JSON.pauses_percentage);
            insert.Parameters.AddWithValue("@agent_overtalk_turns", (cers_stats_JSON.overtalk != null ? cers_stats_JSON.overtalk.agent.turn_count : 0) );
            insert.Parameters.AddWithValue("@agent_overtalk_time", (cers_stats_JSON.overtalk != null ? cers_stats_JSON.overtalk.agent.turn_seconds : 0) );
            insert.Parameters.AddWithValue("@agent_overtalk_percentage", (cers_stats_JSON.overtalk != null ? cers_stats_JSON.overtalk.agent.turn_percentage : 0));
            insert.Parameters.AddWithValue("@caller_overtalk_turns", (cers_stats_JSON.overtalk != null ? cers_stats_JSON.overtalk.caller.turn_count : 0));
            insert.Parameters.AddWithValue("@caller_overtalk_time", (cers_stats_JSON.overtalk != null ? cers_stats_JSON.overtalk.caller.turn_seconds : 0));
            insert.Parameters.AddWithValue("@caller_overtalk_percentage", (cers_stats_JSON.overtalk != null ? cers_stats_JSON.overtalk.caller.turn_percentage : 0));
            insert.Parameters.AddWithValue("@agent_pauses_turns", (cers_stats_JSON.pauses != null ? cers_stats_JSON.pauses.agent.turn_count : 0));
            insert.Parameters.AddWithValue("@agent_pauses_time", (cers_stats_JSON.pauses != null ? cers_stats_JSON.pauses.agent.turn_seconds : 0));
            insert.Parameters.AddWithValue("@agent_pauses_percentage", (cers_stats_JSON.pauses != null ? cers_stats_JSON.pauses.agent.turn_percentage : 0));
            insert.Parameters.AddWithValue("@caller_pauses_turns", (cers_stats_JSON.pauses != null ? cers_stats_JSON.pauses.caller.turn_count : 0));
            insert.Parameters.AddWithValue("@caller_pauses_time", (cers_stats_JSON.pauses != null ? cers_stats_JSON.pauses.caller.turn_seconds : 0));
            insert.Parameters.AddWithValue("@caller_pauses_percentage", (cers_stats_JSON.pauses != null ? cers_stats_JSON.pauses.caller.turn_percentage : 0));
            insert.Parameters.AddWithValue("@agent_words_per_minute", (cers_stats_JSON.speaker_words_per_minute != null ? cers_stats_JSON.speaker_words_per_minute.agent : 0));
            insert.Parameters.AddWithValue("@caller_words_per_minute", (cers_stats_JSON.speaker_words_per_minute != null ? cers_stats_JSON.speaker_words_per_minute.caller : 0));
            insert.Parameters.AddWithValue("@mono_words_per_minute", (cers_stats_JSON.speaker_words_per_minute != null ? cers_stats_JSON.speaker_words_per_minute.mono : 0));
            insert.Parameters.AddWithValue("@agent_words_per_sentence", (cers_stats_JSON.speaker_words_per_sentence != null ? cers_stats_JSON.speaker_words_per_sentence.agent : 0));
            insert.Parameters.AddWithValue("@caller_words_per_sentence", (cers_stats_JSON.speaker_words_per_sentence != null ? cers_stats_JSON.speaker_words_per_sentence.caller : 0));
            insert.Parameters.AddWithValue("@mono_words_per_sentence", (cers_stats_JSON.speaker_words_per_sentence != null ? cers_stats_JSON.speaker_words_per_sentence.mono : 0));

            string tmp = insert.CommandText.ToString();
            foreach (NpgsqlParameter p in insert.Parameters)
            {
                tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
            }

            Console.WriteLine(tmp);

            insert.ExecuteNonQuery();

            if (cers_stats_JSON.speaker_change != null)
            {

                foreach (var item in cers_stats_JSON.speaker_change.agent)
                {
                    NpgsqlCommand insturn = new NpgsqlCommand();
                    insturn.Connection = ekkoDbConnection;
                    insturn.CommandText = "INSERT INTO ceres_speaker_turn " +
                                               " (ceres_process_id, speaker, turn_index, \"timestamp\", relative_timestamp, speaker_change_duration, speaker_change_type) " +
                                               " VALUES " +
                                               " (@refId, @speaker, @turn_index, @timestamp, @relative_timestamp, @change_duration, @change_type) ";
                    insturn.Parameters.AddWithValue("@refId", ceres_process_id);
                    insturn.Parameters.AddWithValue("@speaker", "agent");
                    insturn.Parameters.AddWithValue("@turn_index", item.turn_index);
                    insturn.Parameters.AddWithValue("@timestamp", item.timestamp);
                    insturn.Parameters.AddWithValue("@relative_timestamp", item.timestamp);
                    insturn.Parameters.AddWithValue("@change_duration", item.speaker_change_duration_sec);
                    insturn.Parameters.AddWithValue("@change_type", item.speaker_change_type);
                    insturn.ExecuteNonQuery();

                }

                foreach (var item in cers_stats_JSON.speaker_change.caller)
                {
                    NpgsqlCommand insturn = new NpgsqlCommand();
                    insturn.Connection = ekkoDbConnection;
                    insturn.CommandText = "INSERT INTO ceres_speaker_turn " +
                                               " (ceres_process_id, speaker, turn_index, \"timestamp\", relative_timestamp, speaker_change_duration, speaker_change_type) " +
                                               " VALUES " +
                                               " (@refId, @speaker, @turn_index, @timestamp, @relative_timestamp,  @change_duration, @change_type) ";
                    insturn.Parameters.AddWithValue("@refId", ceres_process_id);
                    insturn.Parameters.AddWithValue("@speaker", "caller");
                    insturn.Parameters.AddWithValue("@turn_index", item.turn_index);
                    insturn.Parameters.AddWithValue("@timestamp", item.timestamp);
                    insturn.Parameters.AddWithValue("@relative_timestamp", item.timestamp);
                    insturn.Parameters.AddWithValue("@change_duration", item.speaker_change_duration_sec);
                    insturn.Parameters.AddWithValue("@change_type", item.speaker_change_type);
                    insturn.ExecuteNonQuery();

                }
            }
        }

            static Guid InsertCeresProcess (NpgsqlConnection myConnection, Guid CeresRunId, string ceres_type)
        {
            Guid process_id = Guid.NewGuid();
            NpgsqlCommand insert_process = new NpgsqlCommand();
            insert_process.Connection = myConnection;
            insert_process.CommandText = "INSERT INTO ceres_process " +
                                        "(id, ceres_run_id, ceres_process_type)   " +
                                          "VALUES ( @processId, @runId, '" + ceres_type + "' ) ";
            insert_process.Parameters.AddWithValue("@processId", process_id);
            insert_process.Parameters.AddWithValue("@runId", CeresRunId);
            insert_process.ExecuteNonQuery();

            return process_id;
        }
        static void InsertTopics(NpgsqlConnection myConnection, Guid process_id, string dir)
        {
                    string JSON = File.ReadAllText(dir + @"\topics.json");

                    var json_top = Newtonsoft.Json.Linq.JObject.Parse(JSON);

                    foreach (var Node in json_top)
                    {
                        if (Node.Key == "topics")
                        {
                            var topics = Newtonsoft.Json.Linq.JObject.Parse(Node.Value.ToString());
                            foreach (var topic in topics)
                            {
                               NpgsqlCommand insert = new NpgsqlCommand();
                               insert.Connection = myConnection;
                               insert.CommandText = "INSERT INTO ceres_topic " +
                                                   "(ceres_process_id " +
                                                     ",topic_id " +
                                                     ",probability ) VALUES " +
                                                     "(@ceres_process_id " +
                                                     ",@topic_id" +
                                                     ",@confidence )";
                               insert.Parameters.AddWithValue("@ceres_process_id", process_id);
                               string top = topic.Key;
                               if (top != "model_name")
                               {
                                   float conf = Convert.ToSingle(topic.Value);

                                   insert.Parameters.AddWithValue("@topic_id", Convert.ToInt32(top));
                                   insert.Parameters.AddWithValue("@confidence", conf);
                                   insert.ExecuteNonQuery();
                               }

                            }
                        }


                    }

        }

        static void LoadRuleData(NpgsqlConnection myConnection, Guid process_id, string dir)
        {
            string JSON = File.ReadAllText(dir + @"\quality.json");
            var json_searializer = new JavaScriptSerializer();
            quality cers_quality_JSON = json_searializer.Deserialize<quality>(JSON);
            if (cers_quality_JSON.mono != null)
            {
                foreach (var item in cers_quality_JSON.mono)
                {
                    foreach (var i in item.Value)
                    {
                        NpgsqlCommand insert = new NpgsqlCommand();
                        insert.Connection = myConnection;
                        insert.CommandText = "INSERT INTO ceres_category_match " +
                                                    " (ceres_process_id, cfg_category_rule_id, match_phrase, actual_slop, position ) " +
                                                    " VALUES " +
                                                    " (@refId, @ruleId, @match_phrase, @slop, @position) ";
                        insert.Parameters.AddWithValue("@refId", process_id);
                        insert.Parameters.AddWithValue("@ruleId", i.query_id);
                        insert.Parameters.AddWithValue("@match_phrase", i.match);
                        insert.Parameters.AddWithValue("@slop", i.actual_slop);
                        insert.Parameters.AddWithValue("@position", i.time);
                        insert.ExecuteNonQuery();
                    }

                }
            }

            if (cers_quality_JSON.agent != null)
            {
                foreach (var item in cers_quality_JSON.agent)
                {
                    foreach (var i in item.Value)
                    {
                        NpgsqlCommand insert = new NpgsqlCommand();
                        insert.Connection = myConnection;
                        insert.CommandText = "INSERT INTO ceres_category_match " +
                                                    " (ceres_process_id, cfg_category_rule_id, match_phrase, actual_slop, position ) " +
                                                    " VALUES " +
                                                    " (@refId, @ruleId, @match_phrase, @slop, @position) ";
                        insert.Parameters.AddWithValue("@refId", process_id);
                        insert.Parameters.AddWithValue("@ruleId", i.query_id);
                        insert.Parameters.AddWithValue("@match_phrase", i.match);
                        insert.Parameters.AddWithValue("@slop", i.actual_slop);
                        insert.Parameters.AddWithValue("@position", i.time);
                        insert.ExecuteNonQuery();
                    }

                }
            }

            if (cers_quality_JSON.caller != null)
            {
                foreach (var item in cers_quality_JSON.caller)
                {
                    foreach (var i in item.Value)
                    {
                        NpgsqlCommand insert = new NpgsqlCommand();
                        insert.Connection = myConnection;
                        insert.CommandText = "INSERT INTO ceres_category_match " +
                                                    " (ceres_process_id, cfg_category_rule_id, match_phrase, actual_slop, position ) " +
                                                    " VALUES " +
                                                    " (@refId, @ruleId, @match_phrase, @slop, @position) ";
                        insert.Parameters.AddWithValue("@refId", process_id);
                        insert.Parameters.AddWithValue("@ruleId", i.query_id);
                        insert.Parameters.AddWithValue("@match_phrase", i.match);
                        insert.Parameters.AddWithValue("@slop", i.actual_slop);
                        insert.Parameters.AddWithValue("@position", i.time);
                        insert.ExecuteNonQuery();
                    }

                }
            }


        }

        static void LoadTranscriptWord(string infile, NpgsqlConnection ekkoDbConnection, Guid transcript_id)
        {
            if (File.Exists(infile))
            {
                string JSON = File.ReadAllText(infile);
                var json_serializer = new JavaScriptSerializer();
                Media VBJson = json_serializer.Deserialize<Media>(JSON);

                // Add in the words and speakers
                int AgentTalkTime = 0;
                int ClientTalkTime = 0;
                int Silence = 0;
                int Overtalk = 0;
                double AgentTalkSpeed;
                double ClientTalkSpeed;

                string speaker = "Any";  // flag for agent or client
                int pstart = 0;  // start of last word spoken.
                int pend = 0;    // end of last word spoken. 
                int sStart = 0;  // current speaker start time
                int sEnd = 0;
                int aWords = 0;  // agent word count
                int cWords = 0;  // client word count
                bool newTurn = true;
                Guid agentGuid = Guid.NewGuid();
                bool agentSet = false;
                Guid callerGuid = Guid.NewGuid();
                bool callerSet = false;
                Guid speakerGuid = agentGuid;
                List<List<string>> agList = new List<List<string>>();
                List<List<string>> clList = new List<List<string>>();
                List<string> wList = new List<string>();

                // set up word isertion query
                NpgsqlCommand insertWd = new NpgsqlCommand();
                insertWd.Connection = ekkoDbConnection;
                insertWd.CommandText = "INSERT INTO transcript_word " +
                                           "(word_id " +
                                           ",transcript_id " +
                                           ",\"position\" " +
                                           ",word " +
                                           ",metadata " +
                                           ",speaker_id " +
                                           ",\"start\" " +
                                           ",\"end\" " +
                                           ",confidence " +
                                           ",volume )" +
                                     "VALUES " +
                                           "(@word_id " +
                                           ",@transcript_id " +
                                           ",@position " +
                                           ",@word " +
                                           ",@metadata " +
                                           ",@speaker_id " +
                                           ",@start " +
                                           ",@end" +
                                           ",@confidence " +
                                           ",@volume ) ";
                insertWd.Parameters.AddWithValue("@transcript_id", NpgsqlTypes.NpgsqlDbType.Uuid, transcript_id);
                insertWd.Parameters.Add("@word_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insertWd.Parameters.Add("@position", NpgsqlTypes.NpgsqlDbType.Integer);
                insertWd.Parameters.Add("@word", NpgsqlTypes.NpgsqlDbType.Varchar);
                insertWd.Parameters.Add("@metadata", NpgsqlTypes.NpgsqlDbType.Varchar);
                insertWd.Parameters.Add("@speaker_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insertWd.Parameters.Add("@start", NpgsqlTypes.NpgsqlDbType.Integer);
                insertWd.Parameters.Add("@end", NpgsqlTypes.NpgsqlDbType.Integer);
                insertWd.Parameters.Add("@confidence", NpgsqlTypes.NpgsqlDbType.Real);
                insertWd.Parameters.Add("@volume", NpgsqlTypes.NpgsqlDbType.Real);
                insertWd.Prepare();


                // set up Frequency isertion query
                NpgsqlCommand insertFq = new NpgsqlCommand();
                insertFq.Connection = ekkoDbConnection;
                insertFq.CommandText = "INSERT INTO transcript_word_freq " +
                                           "(frequency_id " +
                                           ",word_id " +
                                           ",frequency " +
                                           ",energy " +
                                           ",position)" +
                                     "VALUES " +
                                           "(@frequency_id " +
                                           ",@word_id " +
                                           ",@frequency " +
                                           ",@energy " +
                                           ",@position ) ";
                insertFq.Parameters.Add("@frequency_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insertFq.Parameters.Add("@word_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insertFq.Parameters.Add("@frequency", NpgsqlTypes.NpgsqlDbType.Real);
                insertFq.Parameters.Add("@energy", NpgsqlTypes.NpgsqlDbType.Real);
                insertFq.Parameters.Add("@position", NpgsqlTypes.NpgsqlDbType.Integer);
                insertFq.Prepare();

                if (VBJson.media.transcripts.latest.words.Count<Words>() > 0)
                {
                    Words w = VBJson.media.transcripts.latest.words[0];
                    if ((w.m == null) || (w.m != "turn"))  // Assume Mono recording
                    {
                        NpgsqlCommand insSpeaker = new NpgsqlCommand();
                        insSpeaker.Connection = ekkoDbConnection;
                        insSpeaker.CommandText = "insert into transcript_speaker " +
                                                    "(speaker_id, transcript_id, standard_name, custom_name, channel) " +
                                                    "VALUES ('" + speakerGuid + "','" + transcript_id + "','Any','Any', 'mono')";
                        insSpeaker.ExecuteNonQuery();
                        agentSet = true;
                        speakerGuid = agentGuid;
                    }
                }



                foreach (Words word in VBJson.media.transcripts.latest.words)
                {

                    if (word.m != null)
                    {
                        if (word.m == "turn")  // start of new speaker speaking
                        {
                            newTurn = true;
                            if (wList.Count > 0)
                            {
                                List<string> cList = new List<string>(wList);
                                if (speaker == "Agent")   //
                                {
                                    speakerGuid = agentGuid;
                                    agList.Add(cList);
                                }
                                else
                                {
                                    clList.Add(cList);
                                    speakerGuid = callerGuid;
                                }
                                wList.Clear();
                            }

                            if (word.w.Length > 2 && word.w.Substring(0, 5) == "Agent")
                            {
                                speaker = "Agent";
                                if (!agentSet)
                                {
                                    NpgsqlCommand insSpeaker = new NpgsqlCommand();
                                    insSpeaker.Connection = ekkoDbConnection;
                                    insSpeaker.CommandText = "insert into transcript_speaker " +
                                                                "(speaker_id, transcript_id, standard_name, custom_name, channel) " +
                                                                "VALUES ('" + agentGuid + "','" + transcript_id + "','Agent','" + @word.w + "', 'left')";
                                    insSpeaker.ExecuteNonQuery();
                                    agentSet = true;

                                }
                                speakerGuid = agentGuid;

                            }
                            else
                            {
                                speaker = "Caller";
                                if (!callerSet)
                                {
                                    NpgsqlCommand insSpeaker = new NpgsqlCommand();
                                    insSpeaker.Connection = ekkoDbConnection;
                                    insSpeaker.CommandText = "insert into transcript_speaker " +
                                                                "(speaker_id, transcript_id, standard_name, custom_name, channel) " +
                                                                "VALUES ('" + callerGuid + "','" + transcript_id + "','Caller','" + @word.w + "', 'right')";
                                    insSpeaker.ExecuteNonQuery();
                                    callerSet = true;
                                }
                                speakerGuid = callerGuid;


                                sEnd = pend;  // set the end time of the previous speaker to check for overtalk
                            }
                        }
                        else if (word.m == "punc")
                        {
                            List<string> cList = new List<string>(wList);
                            if (speaker == "Agent")
                            {
                                agList.Add(cList);
                            }
                            else
                            {
                                clList.Add(cList);
                            }
                            wList.Clear();
                        }
                    }
                    else
                    {
                        if ((word.s - pend) > 2000)  // if more than 2 seconds since last word count as silence.
                        {
                            Silence = Silence + (word.s - pend);
                        }
                        if (word.s < sEnd)   // check if spoken word is before the last speaker has finished.
                        {
                            Overtalk = Overtalk + word.e - word.s;
                        }

                        if (speaker == "Agent")  // add words and talk time to speaker
                        {
                            aWords++;
                            AgentTalkTime = (newTurn ? AgentTalkTime + word.e - word.s : AgentTalkTime + word.e - pend);
                        }
                        else
                        {
                            cWords++;
                            ClientTalkTime = (newTurn ? ClientTalkTime + word.e - word.s : ClientTalkTime + word.e - pend);
                        }

                        newTurn = false;
                        wList.Add(word.w.ToLower());

                        pend = word.e;
                        // insert into words and frequency



                    }

                    Guid wordId = Guid.NewGuid();
                    insertWd.Parameters["@word_id"].Value = wordId;
                    insertWd.Parameters["@position"].Value = word.p;
                    insertWd.Parameters["@word"].Value = word.w;
                    insertWd.Parameters["@metadata"].Value = (word.m != null ? word.m : "");
                    insertWd.Parameters["@speaker_id"].Value = speakerGuid;
                    insertWd.Parameters["@start"].Value = word.s;
                    insertWd.Parameters["@end"].Value = word.e;
                    insertWd.Parameters["@confidence"].Value = word.c;
                    insertWd.Parameters["@volume"].Value = word.v;
                    insertWd.ExecuteNonQuery();

                    int fPos = 0;
                    // insert into frequency
                    /*                   if (word.frq != null)
                                       {
                                           foreach (Frqs fqcy in word.frq)
                                           {
                                               fPos++;
                                               Guid frqId = Guid.NewGuid();
                                               insertFq.Parameters["@frequency_id"].Value = Guid.NewGuid();
                                               insertFq.Parameters["@word_id"].Value = wordId;
                                               insertFq.Parameters["@frequency"].Value = fqcy.f;
                                               insertFq.Parameters["@energy"].Value = fqcy.e;
                                               insertFq.Parameters["@position"].Value = fPos;
                                               insertFq.ExecuteNonQuery();
                                           }
                                       } */


                }
            }
        }



        static void LoadTranscript(string infile, NpgsqlConnection ekkoDbConnection, int recordingFileId, string playFile)
        {
            if (File.Exists(infile))
            {
                string JSON = File.ReadAllText(infile);
                var json_serializer = new JavaScriptSerializer();
                Media VBJson = json_serializer.Deserialize<Media>(JSON);
                Guid transcript_id = Guid.NewGuid();

                NpgsqlCommand insert = new NpgsqlCommand();
                insert.Connection = ekkoDbConnection;
                insert.CommandText = "INSERT INTO transcript " +
                                           "(id " +
                                           ",cfg_transcript_id " +
                                           ",recording_file_id " +
                                           ",ref_id " +
                                           ",submit_time " +
                                           ",status " +
                                           ",complete_time " +
                                           ",source " +
                                           ",engine_version " +
                                           ",source_file_url " +
                                           ",play_file_url " +
                                           ",source_file_hash)" +
                                     "VALUES " +
                                           "(@transcript_id " +
                                           ",@cfg_transcript_id " +
                                           ",@recording_file_id " +
                                           ",@ref_id " +
                                           ",@submit_time " +
                                           ",@status " +
                                           ",@complete_time " +
                                           ",@JSON" +
                                           ",@engine_version " +
                                           ",@source_file_url " +
                                           ",@play_file_url " +
                                           ",@source_file_hash) ";
                insert.Parameters.AddWithValue("@transcript_id", transcript_id);
                insert.Parameters.AddWithValue("@cfg_transcript_id", Guid.Parse("392dffac-6066-11e8-bc77-732621f01ca7"));
                insert.Parameters.AddWithValue("@recording_file_id", recordingFileId);
                insert.Parameters.AddWithValue("@ref_id", VBJson.media.mediaId);
                insert.Parameters.AddWithValue("@submit_time", DateTime.Parse(VBJson.media.dateCreated));
                insert.Parameters.AddWithValue("@status", "active");
                insert.Parameters.AddWithValue("@complete_time", DateTime.Parse(VBJson.media.dateFinished));
                NpgsqlParameter pJSON = new NpgsqlParameter();
                pJSON.Value = JSON;
                pJSON.ParameterName = "JSON";
                pJSON.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json;
                insert.Parameters.Add(pJSON);
                //           insert.Parameters.AddWithValue("@JSON",(readerTQ.GetString(readerTQ.GetOrdinal("JSON"))));
                insert.Parameters.AddWithValue("@engine_version", "v2");
                insert.Parameters.AddWithValue("@source_file_url", playFile);
                insert.Parameters.AddWithValue("@play_file_url", playFile);
                insert.Parameters.AddWithValue("@source_file_hash", Guid.NewGuid());
                insert.ExecuteNonQuery();

                // Add in the words and speakers
                int AgentTalkTime = 0;
                int ClientTalkTime = 0;
                int Silence = 0;
                int Overtalk = 0;
                double AgentTalkSpeed;
                double ClientTalkSpeed;

                string speaker = "Any";  // flag for agent or client
                int pstart = 0;  // start of last word spoken.
                int pend = 0;    // end of last word spoken. 
                int sStart = 0;  // current speaker start time
                int sEnd = 0;
                int aWords = 0;  // agent word count
                int cWords = 0;  // client word count
                bool newTurn = true;
                Guid agentGuid = Guid.NewGuid();
                bool agentSet = false;
                Guid callerGuid = Guid.NewGuid();
                bool callerSet = false;
                Guid speakerGuid = agentGuid;
                List<List<string>> agList = new List<List<string>>();
                List<List<string>> clList = new List<List<string>>();
                List<string> wList = new List<string>();

                // set up word isertion query
                NpgsqlCommand insertWd = new NpgsqlCommand();
                insertWd.Connection = ekkoDbConnection;
                insertWd.CommandText = "INSERT INTO transcript_word " +
                                           "(id " +
                                           ",transcript_id " +
                                           ",\"position\" " +
                                           ",word " +
                                           ",metadata " +
                                           ",speaker_id " +
                                           ",\"start\" " +
                                           ",\"end\" " +
                                           ",confidence " +
                                           ",volume )" +
                                     "VALUES " +
                                           "(@word_id " +
                                           ",@transcript_id " +
                                           ",@position " +
                                           ",@word " +
                                           ",@metadata " +
                                           ",@speaker_id " +
                                           ",@start " +
                                           ",@end" +
                                           ",@confidence " +
                                           ",@volume ) ";
                insertWd.Parameters.AddWithValue("@transcript_id", NpgsqlTypes.NpgsqlDbType.Uuid, transcript_id);
                insertWd.Parameters.Add("@word_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insertWd.Parameters.Add("@position", NpgsqlTypes.NpgsqlDbType.Integer);
                insertWd.Parameters.Add("@word", NpgsqlTypes.NpgsqlDbType.Varchar);
                insertWd.Parameters.Add("@metadata", NpgsqlTypes.NpgsqlDbType.Varchar);
                insertWd.Parameters.Add("@speaker_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insertWd.Parameters.Add("@start", NpgsqlTypes.NpgsqlDbType.Integer);
                insertWd.Parameters.Add("@end", NpgsqlTypes.NpgsqlDbType.Integer);
                insertWd.Parameters.Add("@confidence", NpgsqlTypes.NpgsqlDbType.Real);
                insertWd.Parameters.Add("@volume", NpgsqlTypes.NpgsqlDbType.Real);
                insertWd.Prepare();


                // set up Frequency isertion query
                NpgsqlCommand insertFq = new NpgsqlCommand();
                insertFq.Connection = ekkoDbConnection;
                insertFq.CommandText = "INSERT INTO transcript_word_freq " +
                                           "(id " +
                                           ",word_id " +
                                           ",frequency " +
                                           ",energy " +
                                           ",position)" +
                                     "VALUES " +
                                           "(@frequency_id " +
                                           ",@word_id " +
                                           ",@frequency " +
                                           ",@energy " +
                                           ",@position ) ";
                insertFq.Parameters.Add("@frequency_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insertFq.Parameters.Add("@word_id", NpgsqlTypes.NpgsqlDbType.Uuid);
                insertFq.Parameters.Add("@frequency", NpgsqlTypes.NpgsqlDbType.Real);
                insertFq.Parameters.Add("@energy", NpgsqlTypes.NpgsqlDbType.Real);
                insertFq.Parameters.Add("@position", NpgsqlTypes.NpgsqlDbType.Integer);
                insertFq.Prepare();

                if (VBJson.media.transcripts.latest.words.Count<Words>() > 0)
                {
                    Words w = VBJson.media.transcripts.latest.words[0];
                    if ((w.m == null) || (w.m != "turn"))  // Assume Mono recording
                    {
                        NpgsqlCommand insSpeaker = new NpgsqlCommand();
                        insSpeaker.Connection = ekkoDbConnection;
                        insSpeaker.CommandText = "insert into transcript_speaker " +
                                                    "(id, transcript_id, standard_name, custom_name, channel) " +
                                                    "VALUES ('" + speakerGuid + "','" + transcript_id + "','Any','Any', 'mono')";
                        insSpeaker.ExecuteNonQuery();
                        agentSet = true;
                        speakerGuid = agentGuid;
                    }
                }



                foreach (Words word in VBJson.media.transcripts.latest.words)
                {

                    if (word.m != null)
                    {
                        if (word.m == "turn")  // start of new speaker speaking
                        {
                            newTurn = true;
                            if (wList.Count > 0)
                            {
                                List<string> cList = new List<string>(wList);
                                if (speaker == "Agent")   //
                                {
                                    speakerGuid = agentGuid;
                                    agList.Add(cList);
                                }
                                else
                                {
                                    clList.Add(cList);
                                    speakerGuid = callerGuid;
                                }
                                wList.Clear();
                            }

                            if (word.w.Length > 2 && word.w.Substring(0, 5) == "Agent")
                            {
                                speaker = "Agent";
                                if (!agentSet)
                                {
                                    NpgsqlCommand insSpeaker = new NpgsqlCommand();
                                    insSpeaker.Connection = ekkoDbConnection;
                                    insSpeaker.CommandText = "insert into transcript_speaker " +
                                                                "(id, transcript_id, standard_name, custom_name, channel) " +
                                                                "VALUES ('" + agentGuid + "','" + transcript_id + "','Agent','" + @word.w + "', 'left')";
                                    insSpeaker.ExecuteNonQuery();
                                    agentSet = true;
                                    speakerGuid = agentGuid;

                                }
                            }
                            else
                            {
                                speaker = "Caller";
                                if (!callerSet)
                                {
                                    NpgsqlCommand insSpeaker = new NpgsqlCommand();
                                    insSpeaker.Connection = ekkoDbConnection;
                                    insSpeaker.CommandText = "insert into transcript_speaker " +
                                                                "(id, transcript_id, standard_name, custom_name, channel) " +
                                                                "VALUES ('" + callerGuid + "','" + transcript_id + "','Caller','" + @word.w + "', 'right')";
                                    insSpeaker.ExecuteNonQuery();
                                    callerSet = true;
                                    speakerGuid = callerGuid;
                                }


                                sEnd = pend;  // set the end time of the previous speaker to check for overtalk
                            }
                        }
                        else if (word.m == "punc")
                        {
                            List<string> cList = new List<string>(wList);
                            if (speaker == "Agent")
                            {
                                agList.Add(cList);
                            }
                            else
                            {
                                clList.Add(cList);
                            }
                            wList.Clear();
                        }
                    }
                    else
                    {
                        if ((word.s - pend) > 2000)  // if more than 2 seconds since last word count as silence.
                        {
                            Silence = Silence + (word.s - pend);
                        }
                        if (word.s < sEnd)   // check if spoken word is before the last speaker has finished.
                        {
                            Overtalk = Overtalk + word.e - word.s;
                        }

                        if (speaker == "Agent")  // add words and talk time to speaker
                        {
                            aWords++;
                            AgentTalkTime = (newTurn ? AgentTalkTime + word.e - word.s : AgentTalkTime + word.e - pend);
                        }
                        else
                        {
                            cWords++;
                            ClientTalkTime = (newTurn ? ClientTalkTime + word.e - word.s : ClientTalkTime + word.e - pend);
                        }

                        newTurn = false;
                        wList.Add(word.w.ToLower());

                        pend = word.e;
                        // insert into words and frequency



                    }

                    Guid wordId = Guid.NewGuid();
                    insertWd.Parameters["@word_id"].Value = wordId;
                    insertWd.Parameters["@position"].Value = word.p;
                    insertWd.Parameters["@word"].Value = word.w;
                    insertWd.Parameters["@metadata"].Value = (word.m != null ? word.m : "");
                    insertWd.Parameters["@speaker_id"].Value = speakerGuid;
                    insertWd.Parameters["@start"].Value = word.s;
                    insertWd.Parameters["@end"].Value = word.e;
                    insertWd.Parameters["@confidence"].Value = word.c;
                    insertWd.Parameters["@volume"].Value = word.v;
                    insertWd.ExecuteNonQuery();

                    int fPos = 0;
                    // insert into frequency
 /*                   if (word.frq != null)
                    {
                        foreach (Frqs fqcy in word.frq)
                        {
                            fPos++;
                            Guid frqId = Guid.NewGuid();
                            insertFq.Parameters["@frequency_id"].Value = Guid.NewGuid();
                            insertFq.Parameters["@word_id"].Value = wordId;
                            insertFq.Parameters["@frequency"].Value = fqcy.f;
                            insertFq.Parameters["@energy"].Value = fqcy.e;
                            insertFq.Parameters["@position"].Value = fPos;
                            insertFq.ExecuteNonQuery();
                        }
                    } */


                }
            }
        }

   /*     static void LoadDimetro(string inFile, NpgsqlConnection ekkoDbConnection)
        {
            int metaDataId = 0;
            int recordingFileId = 0;

            bool isheader = true;
            var reader = new StreamReader(File.OpenRead(@inFile));
            List<string> headers = new List<string>();

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');

                if (isheader)
                {
                    isheader = false;
                    headers = values.ToList();
                }
                else
                {
                    

                    NpgsqlCommand sql = new NpgsqlCommand("SELECT recording_interaction_id from dm_recording_interaction where recording_interaction_id = " + Int32.Parse(values[0]) , ekkoDbConnection);
                    try
                    {
                        int isthere =  (int)sql.ExecuteScalar();
                        metaDataId++;
                        metaDataId++;
                        metaDataId++;
                        recordingFileId++;
                        continue;
                    }
                    catch (Exception e)
                    {

                    }


                    // Insert record into recording interaction table
                    NpgsqlCommand insert = new NpgsqlCommand();
                    try
                    {
                        insert.Connection = ekkoDbConnection;
                        insert.CommandText = "INSERT INTO dm_recording_interaction " +
                                                   "(recording_interaction_id " +
                                                   ",user_id " +
                                                   ",date_time " +
                                                   ",queue_id ) " +
                                             "VALUES " +
                                                   "(@recording_interaction_id " +
                                                   ",@user_id " +
                                                   ",@date_time " +
                                                   ",@queue_id) ";
                        DateTime xtime = DateTime.ParseExact(values[2], "d/MM/yyyy h:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                        insert.Parameters.AddWithValue("@recording_interaction_id", Int32.Parse(values[0]));
                        insert.Parameters.AddWithValue("@user_id", GetUserId(ekkoDbConnection, values[7]));
                        insert.Parameters.AddWithValue("@date_time", xtime);
                        insert.Parameters.AddWithValue("@queue_id", GetQueueId(ekkoDbConnection, values[8]));
                        insert.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                       
                    }

                    metaDataId++;
                    insert_metadata(insert, values[4], values[0], 1, metaDataId);
                    metaDataId++;
                    insert_metadata(insert, values[5], values[0], 2, metaDataId);
                    metaDataId++;
                    insert_metadata(insert, values[6], values[0], 3, metaDataId);

                    // Insert record into recording file table
                    recordingFileId++;
                    insert.CommandText = "INSERT INTO dm_recording_file " +
                           "(recording_file_id " +
                           ",recording_interaction_id " +
                           ",filename " +
                           ",location " +
                           ",length " +
                           ",status )" +
                     "VALUES " +
                           "(@recording_file_id " +
                           ",@recording_interaction_id" +
                           ",@filename " +
                           ",@location " +
                           ",@length " +
                           ",@status) ";
                    insert.Parameters.Clear();
                    insert.Parameters.AddWithValue("@recording_file_id", recordingFileId);
                    insert.Parameters.AddWithValue("@recording_interaction_id", Int32.Parse(values[0]));
                    string sfile = values[9] + ".mp3";
                    string slength = values[3];
                    string[] slenItems = slength.Split(':');
                    int iLength = (Int32.Parse(slenItems[0]) * 60 * 60 * 1000) + (Int32.Parse(slenItems[0]) * 60 * 1000) + (Int32.Parse(slenItems[0]) * 1000);
                    insert.Parameters.AddWithValue("@filename", sfile);
                    insert.Parameters.AddWithValue("@location", @"http://torquexsv1.australiasoutheast.cloudapp.azure.com/News/Datacom/IPA/April2018_mp3");
                    insert.Parameters.AddWithValue("@length", iLength);
                    insert.Parameters.AddWithValue("@status", "active");
                    insert.ExecuteNonQuery();

                    // Insert record into transcript table
                    LoadTranscript(@"P:\News POC June 2016\Output Files\Datacom\IPA\April2018_json\"+values[9]+".json", ekkoDbConnection, recordingFileId, @"http://torquexsv1.australiasoutheast.cloudapp.azure.com/News/Datacom/IPA/April2018_mp3/"+sfile);
                    
                }
            }
        }*/

        public static T Deserialize<T>(string xmlText)
        {
            try
            {
                var stringReader = new System.IO.StringReader(xmlText);
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
            catch
            {
                throw;
            }
        }
        static void LoadDxc(SqlConnection dxcConn, NpgsqlConnection ekkoDbConnection)
        {
            int metaDataId = 0;
            int recordingFileId = 0;

            string[] dirEntries = Directory.GetDirectories(@"P:\News POC June 2016\Output Files\dxc\Input");


            SqlCommand sqls = new SqlCommand("SELECT Filename, mediaId from dxc ", dxcConn);
            SqlDataReader sreader = sqls.ExecuteReader();
            calldata cdata = new calldata();

            if (sreader.HasRows)
            {
                while (sreader.Read())
                {
                    string transcript_id = sreader.GetString(sreader.GetOrdinal("mediaId"));
                    string filename = sreader.GetString(sreader.GetOrdinal("Filename"));
                    bool filefound = false;
                    string directoryname="";
                    //FInd the file
                    foreach (var dir in dirEntries)
                    {
                        string path = dir + @"\" + filename.Replace(".wav", ".xml");
                        if (File.Exists(path))
                        {
                            var serializer = new ConfigurationContainer().Create();
                            string xml = File.ReadAllText(path);
                            cdata = Deserialize<calldata>(xml);
                            directoryname = Path.GetDirectoryName(dir);
                            filefound = true;
                            break;
                        }
                    }
                    if (!filefound)
                    {
                        continue;
                    }

                        NpgsqlCommand sql = new NpgsqlCommand("SELECT recording_interaction_id from dm_recording_interaction where recording_interaction_id = " + cdata.transid, ekkoDbConnection);
                        try
                        {
                            int isthere = (int)sql.ExecuteScalar();
                            metaDataId++;
                            metaDataId++;
                            metaDataId++;
                            recordingFileId++;
                            continue;
                        }
                        catch (Exception e)
                        {

                        }


                        // Insert record into recording interaction table
                        NpgsqlCommand insert = new NpgsqlCommand();
                        try
                        {
                            insert.Connection = ekkoDbConnection;
                            insert.CommandText = "INSERT INTO dm_recording_interaction " +
                                                       "(recording_interaction_id " +
                                                       ",user_id " +
                                                       ",date_time " +
                                                       ",queue_id ) " +
                                                 "VALUES " +
                                                       "(@recording_interaction_id " +
                                                       ",@user_id " +
                                                       ",@date_time " +
                                                       ",@queue_id) ";
                            DateTime xtime = cdata.calldate.Date.Add(cdata.calltime.TimeOfDay);
                            insert.Parameters.AddWithValue("@recording_interaction_id", cdata.transid);
                            insert.Parameters.AddWithValue("@user_id", GetUserId(ekkoDbConnection, cdata));
                            insert.Parameters.AddWithValue("@date_time", xtime);
                            insert.Parameters.AddWithValue("@queue_id", GetQueueId(ekkoDbConnection, cdata.group));
                            insert.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {

                        }
                    Type type = cdata.GetType();
                    System.Reflection.PropertyInfo[] properties = type.GetProperties();
                    foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(cdata))
                    {
                        metaDataId++;
                        insert_metadata(insert, prop.Name, prop.Converter.ConvertToString(prop.GetValue(cdata)), cdata.transid, metaDataId);
                    }

                        // Insert record into recording file table
                        recordingFileId++;
                        insert.CommandText = "INSERT INTO dm_recording_file " +
                               "(id " +
                               ",recording_interaction_id " +
                               ",filename " +
                               ",location " +
                               ",length " +
                               ",status )" +
                         "VALUES " +
                               "(@recording_file_id " +
                               ",@recording_interaction_id" +
                               ",@filename " +
                               ",@location " +
                               ",@length " +
                               ",@status) ";
                        insert.Parameters.Clear();
                        insert.Parameters.AddWithValue("@recording_file_id", recordingFileId);
                        insert.Parameters.AddWithValue("@recording_interaction_id", cdata.transid);
                        insert.Parameters.AddWithValue("@filename", filename);
                        insert.Parameters.AddWithValue("@location", @"http://torquexsv1.australiasoutheast.cloudapp.azure.com/News/dxc/Input/" + directoryname);
                        insert.Parameters.AddWithValue("@length", cdata.duration);
                        insert.Parameters.AddWithValue("@status", "active");
                        insert.ExecuteNonQuery();

                        // Insert record into transcript table
                        LoadTranscript(@"P:\News POC June 2016\Output Files\dxc\JSON\" + transcript_id + ".json", ekkoDbConnection, recordingFileId, @"http://torquexsv1.australiasoutheast.cloudapp.azure.com/News/Datacom/dxc/Input/" + directoryname + @"/" + filename);

                }
            }
        }

        static int GetUserId(NpgsqlConnection ekkoDbConnection, calldata cdata)
        {
            NpgsqlCommand sql = new NpgsqlCommand("SELECT user_id from dm_user where user_id = '" + cdata.userid + "'", ekkoDbConnection);
            try
            {
                return (int)sql.ExecuteScalar();
            }
            catch (Exception e)
            {
                NpgsqlCommand insert = new NpgsqlCommand("Insert into dm_user (user_id, firstname, lastname, username ) " +
                                                            "VALUES " +
                                                            "(@id, @fname, @lname, @uname)", ekkoDbConnection);
                insert.Parameters.AddWithValue("@id", cdata.userid);
                insert.Parameters.AddWithValue("@fname", cdata.userfn);
                insert.Parameters.AddWithValue("@lname", cdata.userln);
                insert.Parameters.AddWithValue("@uname", cdata.userfn + " " + cdata.userln);
                insert.ExecuteNonQuery();
                return cdata.userid;
            }
        }



        static Guid GetTranscriptId(NpgsqlConnection ekkoDbConnection, string ceres_id)
        {
            NpgsqlCommand sql = new NpgsqlCommand("select transcript_id " +
                                                    "from transcript t " +
                                                    "where t.ref_id = '" + ceres_id + "' " , ekkoDbConnection);
            try
            {
                return (Guid)sql.ExecuteScalar();
            }
            catch (Exception e)
            {
                return Guid.Empty;
            }
        }



        static int GetQueueId(NpgsqlConnection ekkoDbConnection, string zone)
        {
            int queue_id;
            NpgsqlCommand sql = new NpgsqlCommand("SELECT queue_id from dm_queue where queue_name = '" + zone + "'", ekkoDbConnection);
            try
            {
                return (int)sql.ExecuteScalar();
            }
            catch (Exception e)
            {
                try
                {
                    sql.CommandText = "SELECT max(queue_id) from dm_queue";
                    queue_id = (int)sql.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    queue_id = 0;
                }
                queue_id++;
                NpgsqlCommand insert = new NpgsqlCommand("Insert into dm_queue (queue_id, queue_name, queue_importing_id ) " +
                                                            "VALUES " +
                                                            "(@id, @name, @id)", ekkoDbConnection);
                insert.Parameters.AddWithValue("@id", queue_id);
                insert.Parameters.AddWithValue("@name", zone);
                insert.ExecuteNonQuery();
                return queue_id;
            }
        }

        static void insert_metadata(NpgsqlCommand insert, string fieldname, string fieldvalue, int interaction_id, int id)
        {
            int metaTypeId = 0;
            NpgsqlCommand sql = new NpgsqlCommand("SELECT metadata_id from dm_metadata where name = '" + fieldname + "'", insert.Connection);
            try
            {
                metaTypeId = (int)sql.ExecuteScalar();

            } catch (Exception e)
            {
                try
                {
                    sql.CommandText = "SELECT max(metadata_id) from dm_metadata";
                    metaTypeId = (int)sql.ExecuteScalar();
                } catch (Exception xe)
                {

                }
                metaTypeId++;
                sql.CommandText = "INSERT into dm_metadata (metadata_id, name, metadata_group_id) " +
                                    " VALUES (@id,@name,@id)";
                sql.Parameters.Clear();
                sql.Parameters.AddWithValue("@id", metaTypeId);
                sql.Parameters.AddWithValue("@name", fieldname);
                sql.ExecuteNonQuery();
            }
            try
            { 
                string metadata_value = fieldvalue;

                insert.CommandText = "INSERT INTO dm_recording_metadata " +
                                           "(recording_metadata_id " +
                                           ",recording_interaction_id " +
                                           ",metadata_id " +
                                           ",metadata_value ) " +
                                     "VALUES " +
                                           "(@recording_metadata_id " +
                                           ",@recording_interaction_id " +
                                           ",@metadata_id " +
                                           ",@metadata_value) ";
                insert.Parameters.Clear();
                insert.Parameters.AddWithValue("@recording_metadata_id", id);
                insert.Parameters.AddWithValue("@recording_interaction_id", interaction_id);
                insert.Parameters.AddWithValue("@metadata_id", metaTypeId);
                insert.Parameters.AddWithValue("@metadata_value", metadata_value);
                insert.ExecuteNonQuery();
            }
            catch (Exception e)
            {

            }

        }

        static DateTime GetTimeStamp(string inUrl)
        {
            if (inUrl.Length > 20)
            {
                string[] splitAr = inUrl.Split('/');
                string[] splitTm = splitAr[8].Split('_');
                DateTime outUrl = DateTime.Parse(splitTm[2] + "-" + splitTm[1] + "-" + splitTm[0] + " " + splitTm[4] + ":" + splitTm[5]);
                return outUrl;
            } else
            {
                return DateTime.Now;
            }
            
        }

    }
}
