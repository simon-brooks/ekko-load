﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekko_Load
{

// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class calldata
    {

        private int transidField;

        private System.DateTime calldateField;

        private System.DateTime calltimeField;

        private int useridField;

        private int agentidField;

        private string vZIDField;

        private string userfnField;

        private string userlnField;

        private int durationField;

        private string groupField;

        private string dnisField;

        private string aniField;

        private string fot_serial_idField;

        private string directionField;

        private int wrap_up_timeField;

        private string sm_session_idField;

        private calldataData[] uudataField;

        private object classificationsField;

        /// <remarks/>
        public int transid
        {
            get
            {
                return this.transidField;
            }
            set
            {
                this.transidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime calldate
        {
            get
            {
                return this.calldateField;
            }
            set
            {
                this.calldateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime calltime
        {
            get
            {
                return this.calltimeField;
            }
            set
            {
                this.calltimeField = value;
            }
        }

        /// <remarks/>
        public int userid
        {
            get
            {
                return this.useridField;
            }
            set
            {
                this.useridField = value;
            }
        }

        /// <remarks/>
        public int agentid
        {
            get
            {
                return this.agentidField;
            }
            set
            {
                this.agentidField = value;
            }
        }

        /// <remarks/>
        public string VZID
        {
            get
            {
                return this.vZIDField;
            }
            set
            {
                this.vZIDField = value;
            }
        }

        /// <remarks/>
        public string userfn
        {
            get
            {
                return this.userfnField;
            }
            set
            {
                this.userfnField = value;
            }
        }

        /// <remarks/>
        public string userln
        {
            get
            {
                return this.userlnField;
            }
            set
            {
                this.userlnField = value;
            }
        }

        /// <remarks/>
        public int duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        public string group
        {
            get
            {
                return this.groupField;
            }
            set
            {
                this.groupField = value;
            }
        }

        /// <remarks/>
        public string dnis
        {
            get
            {
                return this.dnisField;
            }
            set
            {
                this.dnisField = value;
            }
        }

        /// <remarks/>
        public string ani
        {
            get
            {
                return this.aniField;
            }
            set
            {
                this.aniField = value;
            }
        }

        /// <remarks/>
        public string fot_serial_id
        {
            get
            {
                return this.fot_serial_idField;
            }
            set
            {
                this.fot_serial_idField = value;
            }
        }

        /// <remarks/>
        public string direction
        {
            get
            {
                return this.directionField;
            }
            set
            {
                this.directionField = value;
            }
        }

        /// <remarks/>
        public int wrap_up_time
        {
            get
            {
                return this.wrap_up_timeField;
            }
            set
            {
                this.wrap_up_timeField = value;
            }
        }

        /// <remarks/>
        public string sm_session_id
        {
            get
            {
                return this.sm_session_idField;
            }
            set
            {
                this.sm_session_idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("data", IsNullable = false)]
        public calldataData[] uudata
        {
            get
            {
                return this.uudataField;
            }
            set
            {
                this.uudataField = value;
            }
        }

        /// <remarks/>
        public object classifications
        {
            get
            {
                return this.classificationsField;
            }
            set
            {
                this.classificationsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class calldataData
    {

        private string nameField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute(DataType = "integer")]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }



    }

